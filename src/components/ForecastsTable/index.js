import _ from 'lodash';
import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import {Grid, Row} from 'react-flexbox-grid';
import ForecastsHeader from './../ForecastsHeader';
import Forecast from './../Forecast';
import Marquee from './../Marquee';
import {getForecasts} from './../../api/Forecasts';

import style from './main.css';

const COLUMNS_PER_PAGE = 2;
const ROW_PER_PAGE = 3;
const FORECASTS_PER_PAGE = ROW_PER_PAGE * COLUMNS_PER_PAGE;
const PAGES_TIMEOUT = 8000;
const FORECASTS_FETCH_TIMEOUT = 10000;

class ForecastsTable extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        station: PropTypes.string.isRequired,
    };
    static defaultProps = {
    };
    state = {
        forecasts: {
            data: null,
            text: undefined,
        },
        currentPage: 1,
    };

    _switchPagesTimerId;
    _fetchForecastsTimerId;

    _startPagesTimer = () => {
        this._switchPagesTimerId = setInterval(() => {
            const totalForecasts = this.state.forecasts.data && this.state.forecasts.data.length || 0;
            const totalPages = Math.ceil(totalForecasts / FORECASTS_PER_PAGE);
            if (this.state.currentPage >= totalPages) {
                this.setState({
                    currentPage: 1,
                });
            } else {
                this.setState({
                    currentPage: this.state.currentPage + 1,
                });
            }
        }, PAGES_TIMEOUT);
    };

    _startForecastsTimer = () => {
        this._fetchForecastsTimerId = setInterval(() => {
            getForecasts({id: this.props.id})
                .then((forecasts) => {
                    this.setState({
                        forecasts,
                    });
                });
        }, FORECASTS_FETCH_TIMEOUT);
    };

    componentDidMount() {
        getForecasts({id: this.props.id})
            .then((forecasts) => {
                this.setState({
                    forecasts,
                }, () => {
                    if (!_.isNull(forecasts.data)) {
                        this._startForecastsTimer();
                        this._startPagesTimer();
                    }
                });
            });
    }

    componentWillUnmount() {
        if (this._switchPagesTimerId) {
            clearInterval(this._switchPagesTimerId);
        }
        if (this._fetchForecastsTimerId) {
            clearInterval(this._fetchForecastsTimerId);
        }
    }

    constructor(props) {
        super(props);

        this._startPagesTimer = this._startPagesTimer.bind(this);
        this._startForecastsTimer = this._startForecastsTimer.bind(this);
    }

    render() {
        const {forecasts: {data = null, text}} = this.state;
        return (
            <Grid className={style.container}>
                <ForecastsHeader
                    station={this.props.station}
                />
                {
                    _.times(ROW_PER_PAGE).map((rowNumber) => {
                        return (
                            <Row>
                                {
                                    _.times(COLUMNS_PER_PAGE).map((columnNumber) => {
                                        const forecast = _.isNull(data)
                                            ? null
                                            : data[(columnNumber
                                                * ROW_PER_PAGE + rowNumber)
                                                + (this.state.currentPage - 1)
                                                * FORECASTS_PER_PAGE];
                                        return (
                                            <Forecast forecast={forecast} />
                                        );
                                    })
                                }
                            </Row>
                        );
                    })
                }
                <Marquee text={text} />
            </Grid>
        );
    }
}

export default ForecastsTable;
