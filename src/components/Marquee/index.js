/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'react-flexbox-grid';

import style from './main.css';

const Marquee = (props) => {
    return (
        <Row className={style.row}>
            <Col className={style['forecast-footer-cell']} xs>
                <marquee scrollamount='2'>
                    {props.text ? props.text : null}
                </marquee>
            </Col>
        </Row>
    );
};

Marquee.propTypes = {
    text: PropTypes.string.isRequired,
};

export default Marquee;
