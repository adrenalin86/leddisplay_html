import PropTypes from 'prop-types';
import React, {PureComponent} from 'react';
import ForecastsTable from './../ForecastsTable';

import style from './main.css';

class Layout extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        station: PropTypes.string.isRequired,
    };
    static defaultProps = {
    };

    render() {
        return (
            <div className={style.container}>
                <ForecastsTable
                    id={this.props.id}
                    station={this.props.station}
                />
            </div>
        );
    }
}

export default Layout;
