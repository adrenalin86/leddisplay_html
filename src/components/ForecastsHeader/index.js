import PropTypes from 'prop-types';
import cs from 'classnames';
import React, {Fragment, PureComponent} from 'react';
import {Row, Col} from 'react-flexbox-grid';

import style from './main.css';

const DATETIME_TIMEOUT = 1000;

function getCurrentTime() {
    const now = new Date();
    // eslint-disable-next-line
    return ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2) + ':' + ('0' + now.getSeconds()).slice(-2);
}

class ForecastsHeader extends PureComponent {
    static propTypes = {
        station: PropTypes.string.isRequired,
    };
    static defaultProps = {
    };
    state = {
        currentTime: undefined,
    };

    _dateTimeTimerId;

    _startDateTimeTimer() {
        this._dateTimeTimerId = setInterval(() => {
            this.setState({
                currentTime: getCurrentTime(),
            });
        }, DATETIME_TIMEOUT);
    }

    componentDidMount() {
        this._startDateTimeTimer();
    }

    componentWillUnmount() {
        if (this._dateTimeTimerId) {
            clearInterval(this._dateTimeTimerId);
        }
    }

    render() {
        return (
            <Fragment>
                <Row>
                    <Col className={style['table-header-cell']} xs>
                        <b>{this.props.station}</b>&nbsp;
                        <span className={style.datetime}>{this.state.currentTime}</span>
                    </Col>
                </Row>
                <Row>
                    <Col className={style['forecast-header-cell']} xs><b>Маршрут</b></Col>
                    <Col className={cs(style['forecast-header-cell'], style.time)} xs>Время</Col>
                    <Col className={style['forecast-header-cell']} xs><b>Маршрут</b></Col>
                    <Col className={cs(style['forecast-header-cell'], style.time)} xs>Время</Col>
                </Row>
            </Fragment>
        );
    }
}

export default ForecastsHeader;
