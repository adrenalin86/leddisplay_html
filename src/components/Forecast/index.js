import _ from 'lodash';
import cs from 'classnames';
import PropTypes from 'prop-types';
import React, {Fragment, PureComponent} from 'react';
import {Col} from 'react-flexbox-grid';

import lowFloorLogo from './img/lowFloorLogo.png';
import style from './main.css';

const SECONDS_PER_MINUTE = 60;

const getVehicleTitle = (forecast) => {
    return `${forecast.type}-${forecast.num}`;
};

const getVehicleTime = (forecast) => {
    if (forecast.arrTime < SECONDS_PER_MINUTE) {
        return '< 1 мин.';
    }
    return `${Math.ceil(forecast.arrTime / SECONDS_PER_MINUTE)} мин.`;
};

const getVehicleClassByType = (type) => {
    if (type === 'А') {
        return 'bus';
    } else if (type === 'Т') {
        return 'trolley-bus';
    } else {
        return 'car';
    }
};

class Forecast extends PureComponent {
    static propTypes = {
        forecast: PropTypes.object,
    };
    static defaultProps = {
        forecast: null,
    };

    render() {
        const {forecast} = this.props;
        if (_.isNull(forecast)) {
            return (
                <Fragment>
                    <Col className={style['forecast-cell']} xs />
                    <Col className={cs(style['forecast-cell'], style.time)} xs />
                </Fragment>
            );
        }
        return (
            <Fragment>
                <Col className={cs(style['forecast-cell'], style[getVehicleClassByType(forecast.type)])} xs>
                    <b>{getVehicleTitle(forecast)}</b>
                    {!forecast.lowFloor
                        ? null
                        : <img className={style.lowFloor} src={lowFloorLogo} alt="Низкопольный транспорт" />}
                </Col>
                <Col className={cs(style['forecast-cell'], style.time)} xs>{getVehicleTime(forecast)}</Col>
            </Fragment>
        );
    }
}

export default Forecast;
