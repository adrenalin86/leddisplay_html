import 'url-search-params-polyfill';

import React, {Component} from 'react';
import Layout from './components/Layout';

const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id') || '6200100001';
const station = urlParams.get('station') || 'Название остановки';

class App extends Component {
    render() {
        return (
            <Layout
                id={id}
                station={station}
            />
        );
    }
}

export default App;
