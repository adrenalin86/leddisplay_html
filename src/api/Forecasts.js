import axios from 'axios';

const host = process.env.NODE_ENV === 'production' ? 'http://bus62.ru' : '/api';

function getForecasts({id}) {
    return axios.all([axios.get(`${host}/getForecasts.php?id=${id}`)])
        .then((response) => {
            return {
                data: response[0].data.forecasts,
                text: response[0].data.text,
                weather: response[0].data.weather,
            };
        }).catch(() => {
            return {
                data: [],
                text: '',
                weather: null,
            };
        });
}

export {
    getForecasts,
};
